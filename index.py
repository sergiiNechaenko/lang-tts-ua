from flask import Flask, escape, request, Response
from flask_cors import CORS
from gtts import gTTS
import json
import random
# get this object

app = Flask(__name__)
CORS(app)

@app.route('/api/make_sound_from_text',methods=['GET'])
def make_sound_from_text():
	args = request.args
	text = args['text']

	# From Text to Speech
	gttsObject = gTTS(text=text, lang='uk', slow=False, lang_check=True)  
	filename = "sound"+str(random.randint(432423,23423432342))+".mp3";
	gttsObject.save(filename)

	return Response(json.dumps([filename]),  mimetype='application/json')


# from gtts import gTTS 
# import os, time, sys, pygame
# import pyttsx3

# text = "Як умру, то поховайте. Мене на могилі, Серед степу широкого, На Вкраїні милій"

# # From Text to Speech
# gttsObject = gTTS(text=text, lang='uk', slow=False, lang_check=True)   
# gttsObject.save("sound.mp3") 


# pygame.mixer.init()
# # Run sound
# pygame.mixer.music.load('sound.mp3')
# pygame.mixer.music.play()
# time.sleep(10)
# pygame.mixer.music.stop()
